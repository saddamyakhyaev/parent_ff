#!/usr/bin/env bash

if [ -n "$1" ]; then

  if [ "$1" = "development" ];
  then port="1054"
  elif [ "$1" = "mgsu" ];
  then port="1055"
  elif [ "$1" = "misis" ];
  then port="1056"
  elif [ "$1" = "rudn" ];
    then port="1057"

  else
    echo "Not found specified departament in list departaments!"
    exit
  fi

  echo '>>>>>>>>>>>>>> MAVEN CLEAN INSTALL <<<<<<<<<<<<<<<<<<<'
  ./mvnw clean install -Ddepartament=$1

  echo '>>>>>>>>>>>>>> COPY FILE IN SERVER <<<<<<<<<<<<<<<<<<<'
  scp backend_ff/target/$1-departament.jar root@65.21.188.171:/home/jars

  echo '>>>>>>>>>>>>>> RUN JAVA JAR <<<<<<<<<<<<<<<<<<<'
  ssh root@65.21.188.171 << EOF

  fuser -k $port/tcp
  nohup java -jar /home/jars/$1-departament.jar --spring.profiles.active=$1 > log.txt &

EOF
else
  echo "Not specific departament! "
fi

